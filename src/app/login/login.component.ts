import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: {
    username: string;
    password: string;
  } = {username: '', password: '',}

  constructor(
    public _login: LoginService,
    private router: Router
  ) { }



  ngOnInit() {

  }

  login(): void {
    if (this._login.login(this.user.username, this.user.password)) {
      localStorage.setItem('user', JSON.stringify(this.user));
      this.router.navigate(['movie']);
    }
  }
}
