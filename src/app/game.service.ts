import { Injectable } from '@angular/core';
import { Game } from './game.model';

@Injectable()
export class GameService {
  private _games: Game[] = [];
  selectedGame: Game;
  constructor() { }
  get games() {
    return this._games;
  }
  set games(games: Game[]) {
    this._games=games;
  }

  increase(game: Game): void {
    game.rating++;
    this.selectedGame = game;
  }

}
