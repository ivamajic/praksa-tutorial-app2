import { GameService } from './../../game.service';
import { Game } from './../../game.model';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {

  @Input() public game: Game;


  constructor(
    private _game: GameService
  ) { }

  ngOnInit() {
  }

  increase(): void {
    this._game.increase(this.game);
  }



}
