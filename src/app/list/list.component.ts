import { GameService } from './../game.service';
import { Game } from './../game.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public games: Game[] = [];
  lastChanged: string;
  constructor(public _game: GameService) { }

  ngOnInit() {
    for(let i=0; i<5; i++) {
      let game = new Game(i, 'game' + i);
      this.games.push(game);
    }
  }
}
