import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: ['h2 {font-size: 30px; color: red;}']
})
export class AppComponent {
  title = 'app';
  isLoggedIn: boolean = false;
  user: {
    username: string;
    password: string;
  }

  constructor(public _login: LoginService) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    if(this._login.isLoggedIn) {
      this.isLoggedIn = true;
      console.log(this.isLoggedIn);
    }
  }



  logout(): void {
    this._login.logout();
  }


}
