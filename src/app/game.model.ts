export class Game {
  id: number;
  name: string;
  rating: number = 0;

  constructor(id?: number, name?: string) {
    this.id = id;
    this.name = name;
  }
}
