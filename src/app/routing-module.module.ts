import { AuthGuard } from './auth.guard';
import { MovieEditComponent } from './movies/movie-edit/movie-edit.component';
import { LoginComponent } from './login/login.component';
import { MovieListComponent } from './movies/movie-list/movie-list.component';
import { MoviesComponent } from './movies/movies/movies.component';
import { EditComponent } from './edit/edit.component';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full'},
  { path:'list', component: ListComponent},
  { path:'edit', component: EditComponent},
  { path: 'login', component: LoginComponent},
  { path: 'movie', component: MoviesComponent, canActivate: [AuthGuard], children: [
    { path: "list", canActivate: [AuthGuard], component: MovieListComponent},
    { path: "list/:id", component: MovieEditComponent},
    { path: '', redirectTo: 'list', pathMatch: 'full'}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routedComponents = [AppComponent];
