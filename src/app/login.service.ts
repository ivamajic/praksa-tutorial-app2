import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  loggedIn: boolean = false;

  constructor(
    public router: Router
  ) { }

  login(username: string, password: string): boolean {
    if (username == "usernametest" && password == "passwordtest") {
      this.loggedIn = true;
      return true;
    }
    return false;
  }

  isLoggedIn():boolean {
    if (localStorage.getItem('user')) return true;
    else return false;
  }

  logout(): void {
    localStorage.removeItem('user');
    this.loggedIn = false;
    this.router.navigate(['login']);
  }

}
