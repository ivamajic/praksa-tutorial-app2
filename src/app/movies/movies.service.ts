import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';
import { Movie } from './movie.model';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { of } from 'rxjs/observable/of';

@Injectable()
export class MoviesService {
  /** fdgfg */
  private URL: string = '/assets/movies.json';
  constructor(
    private http: HttpClient
  ) { }

  getPosts(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.URL).map(data => {
        let movies = [];
        data.forEach(element => {
          let movie = Object.assign(new Movie, element);
          movies.push(movie);
        });
        return movies;
      })
  }

  insertPost(movie: Movie): Observable<any> {
    return this.http.post(this.URL, movie);
  }
}
