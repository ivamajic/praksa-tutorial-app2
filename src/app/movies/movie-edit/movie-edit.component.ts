import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movie-edit',
  templateUrl: './movie-edit.component.html',
  styleUrls: ['./movie-edit.component.css']
})
export class MovieEditComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public alertMessage(value) {
    alert(value)
  }

}
