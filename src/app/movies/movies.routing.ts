import { MovieListComponent } from './movie-list/movie-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';



const routes: Routes = [
  { path: '', component: MoviesComponent, children: [
    { path: "list", component: MovieListComponent},
    { path: '', redirectTo: 'list', pathMatch: 'full'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovieRoutingModule { }

