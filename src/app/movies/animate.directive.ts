import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appAnimate]'
})
export class AnimateDirective {

  @Input() color: string = "red";

  constructor(
    private el: ElementRef
  ) {

   }

   @HostListener('click', ['$event'])
   onClick() {
    this.el.nativeElement.style.backgroundColor = this.color;
   }

}
