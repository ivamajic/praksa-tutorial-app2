import { LoginService } from './../login.service';
import { MovieRoutingModule } from './movies.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieEditComponent } from './movie-edit/movie-edit.component';
import { MoviesComponent } from './movies/movies.component';
import { MoviesService } from './movies.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { IndexPipe } from './index.pipe';
import { AnimateDirective } from './animate.directive';

@NgModule({
  imports: [
    CommonModule,
    MovieRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [MovieListComponent, MovieEditComponent, MoviesComponent, IndexPipe, AnimateDirective],
  exports: [MovieListComponent, MovieEditComponent, MoviesComponent],
  providers: [
    MoviesService,
    LoginService
  ]
})
export class MoviesModule { }
