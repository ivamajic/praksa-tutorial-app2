import { MovieEditComponent } from './../movie-edit/movie-edit.component';
import { MoviesService } from './../movies.service';
import { Movie } from './../movie.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
  @ViewChild("movieEdit") movieEdit: MovieEditComponent;
  movies: Movie[] = [];
  selectedMovie: Movie;
  copyMovie: Movie = new Movie;

  constructor(
    public _movies: MoviesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  selectMovie(movie: Movie): void {
    this.selectedMovie = movie;
    Object.assign(this.copyMovie, this.selectedMovie);
  }

  update(): void {
    Object.assign(this.selectedMovie, this.copyMovie);
    this.selectedMovie = null;
    this.movieEdit.alertMessage("this is alert")
  }

  cancel(): void {
    this.selectedMovie = null;
  }

  getMovies(): void {
    this._movies.getPosts()
      .subscribe(
        data => {
          this.movies = data;
        }

      )
  }

  insert(): void {
    let movie = new Movie;
    movie.body = "asdasd";
    movie.title = "dfgdfgdfgdfg";
    movie.userId = 1;
    this._movies.insertPost(movie)
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.error(error);
        }
      )
  }
}
