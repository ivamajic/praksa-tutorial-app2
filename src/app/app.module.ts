import { LoginService } from './login.service';
import { FormsModule } from '@angular/forms';
import { MoviesModule } from './movies/movies.module';
import { GameService } from './game.service';
import { AppRoutingModule } from './routing-module.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { EditComponent } from './edit/edit.component';
import { RatingComponent } from './list/rating/rating.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    EditComponent,
    RatingComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MoviesModule,
    FormsModule
  ],
  providers: [
    GameService,
    LoginService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
